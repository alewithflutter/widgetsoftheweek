import 'package:flutter/material.dart';
import 'package:widgets_ofweek/screens/list.dart';
import 'package:widgets_ofweek/screens/widgetSelected.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      initialRoute: '/',
      routes: {
        '/' : (BuildContext context) => ListOfWidgets(),
        'widget/detail' : (BuildContext context) => WidgetSelected()
      },
    );
  }
}