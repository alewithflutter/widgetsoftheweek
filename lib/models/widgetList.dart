import 'package:flutter/material.dart';

class WidgetList{
  int id;
  String name;
  String videoUrl;
  Widget widget;

  WidgetList({
    this.id,
    this.name,
    this.videoUrl,
    this.widget
  });
  
}