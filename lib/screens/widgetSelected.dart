import 'package:flutter/material.dart';
import 'package:widgets_ofweek/models/widgetList.dart';
class WidgetSelected extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {

    WidgetList widgetList = ModalRoute.of(context).settings.arguments;

    return widgetList.widget;
  }
}