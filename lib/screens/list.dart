import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:widgets_ofweek/models/widgetList.dart';
import 'package:widgets_ofweek/widgets/Expanded.dart';
import 'package:widgets_ofweek/widgets/items/SafeArea.dart';
class ListOfWidgets extends StatelessWidget {
  
  

  @override
  Widget build(BuildContext context) {

    //widget list
    List<WidgetList> widgetsItems = [
      WidgetList(
        name: 'SafeArea Widget',
        videoUrl: 'https://www.youtube.com/watch?v=lkF0TQJO0bA&list=PLjxrf2q8roU23XGwz3Km7sQZFTdB996iG&index=2',
        widget: SafeAreaWidget()
      ),
      WidgetList(
        name: 'Expanded Widget',
        videoUrl: 'https://www.youtube.com/watch?v=_rnZaagadyo&list=PLjxrf2q8roU23XGwz3Km7sQZFTdB996iG&index=3',
        widget: ExpandedWidget()
      ),
    ];

    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              welcomeTitles(context),
              SizedBox(height: 10),
              listOfWidgets(widgetsItems)
            ],
          ),
        ),
      )
    );
  }

  Widget welcomeTitles(BuildContext context){
    return Container(
      padding: EdgeInsets.only(
        top: 30,
        left: 20,
        right: 20,
        bottom: 10
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.6,
            child: Text('Widgets of the Week!', 
              style: GoogleFonts.raleway(
                color: Colors.blue[700],
                fontSize: 30,
                fontWeight: FontWeight.w600
              )
            ),
          ),
          SizedBox(height: 20),
          Text('Todos estos widgets son proporcionados por el canal de Flutter.',
            style: GoogleFonts.raleway(
              color: Colors.grey[500],
              fontSize: 20,
              fontWeight: FontWeight.w600
            )
          ),
        ],
      ),
    );
  }

  Widget listOfWidgets( List<WidgetList> widgetList ){
    return Expanded(
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(
          horizontal: 10
        ),
        child: ListView.builder(
          itemCount: widgetList.length,
          itemBuilder: (BuildContext context, int index){
            return itemCard(context, widgetList[index]);
          },
        ),
      )
    );
  }

  Widget itemCard(BuildContext context ,WidgetList item){
    
    final parseUrl = Uri.parse(item.videoUrl);
    final idVideo = parseUrl.queryParameters['v'];
    
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(5)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 3,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ]
      ),
      padding: EdgeInsets.all(20),
      margin: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: FadeInImage(
              placeholder: AssetImage('assets/loader.gif'), 
              image: NetworkImage('https://img.youtube.com/vi/$idVideo/0.jpg')
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 5
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${item.name}',
                  style: GoogleFonts.raleway(
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                    color: Colors.blue[900]
                  ),
                ),
                SizedBox(height: 15),
                Row(
                  children: [
                    learnMore(context, item),
                    SizedBox(width: 10),
                    goToYoutube(item.videoUrl)
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget learnMore(BuildContext context, WidgetList infoWidget){
    return Material(
          child: InkWell(
          splashColor: Colors.grey[400],
          highlightColor: Colors.grey[400],
          onTap: ()=> Navigator.pushNamed(context, 'widget/detail', arguments: infoWidget),
          child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 5
          ),
          decoration: BoxDecoration(
            color: Colors.blue[400].withOpacity(0.5),
            borderRadius: BorderRadius.circular(5)
          ),
          child: Row(
            children: [
              Text('Aprender más', style: GoogleFonts.raleway(
                color: Colors.blue[900],
                fontWeight: FontWeight.w700
              )),
              SizedBox(width: 10),
              Icon(Icons.arrow_forward, color: Colors.blue[900])
            ],
          ),
        ),
      ),
    );
  }

  Widget goToYoutube(youtubeUrl){
    return Material(
          child: InkWell(
          splashColor: Colors.grey[600],
          highlightColor: Colors.grey[600],
          onTap: ()=> _openYoutubeUrl(youtubeUrl),
          child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 5
          ),
          decoration: BoxDecoration(
            color: Colors.blue[400].withOpacity(0.5),
            borderRadius: BorderRadius.circular(5)
          ),
          child: Row(
            children: [
              Text('Ir a youtube', style: GoogleFonts.raleway(
                color: Colors.blue[900],
                fontWeight: FontWeight.w700
              )),
              SizedBox(width: 10),
              Icon(Icons.movie, color: Colors.blue[900])
            ],
          ),
        ),
      ),
    );
  }

  _openYoutubeUrl(youtubeUrl) async {
    if( Platform.isIOS ){
        if (await canLaunch(youtubeUrl)) {
            await launch(youtubeUrl);
        } else {
          throw 'Could not launch $youtubeUrl';
        }
    } else {
      if (await canLaunch(youtubeUrl)) {
          await launch(youtubeUrl);
      } else {
        throw 'Could not launch $youtubeUrl';
      }
    }
  }
}