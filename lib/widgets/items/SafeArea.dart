import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
class SafeAreaWidget extends StatefulWidget {

  @override
  _SafeAreaWidgetState createState() => _SafeAreaWidgetState();
}

class _SafeAreaWidgetState extends State<SafeAreaWidget> {

  bool showSafeArea = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: showSafeArea ? 
      SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 20
          ),
          child: Text('Ahora si me puedes ver, el SafeArea evita que tu contenido este debajo del notch o barra de estado :)'),
        )
      )
      :
      Container(
        child: Text('Este es el contenido del Scaffold sin SafeArea, no me puedes ver :)'),
      )
      ,
      floatingActionButton: FloatingActionButton(
        onPressed: ()=>_showBottomSheet(context),
        child: Icon(Icons.help)
      ),
    );
  }

  _showBottomSheet(BuildContext context){
    showModalBottomSheet(
      context: context,
      builder: (context){
        return Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('¿Qué es el SafeArea?', style: GoogleFonts.raleway(
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                  color: Colors.blue[700]
                )
              ),
              SizedBox(height: 10),
              Text(!showSafeArea ? 'Aprieta el botón y veras la explicación y su magia 😄.' : 'Perfecto! has visto la magia de este mágico widget que puede ahorrarte un par de cositas 🧙🏼‍♂️', 
                style: GoogleFonts.raleway(
                  fontSize: 20    
                ),
              ),
              SizedBox(height: 10),
              RaisedButton(
                color: Colors.blue[800],
                textColor: Colors.white,
                onPressed: ()=>setState((){
                  showSafeArea = !showSafeArea; 
                  Navigator.pop(context);
                }),
                child: Text(!showSafeArea ? 'Deseo verlo!' : 'Interesante. Volveré a normal'),
              )
            ],
          ),
        );
      }

    );
  }
}