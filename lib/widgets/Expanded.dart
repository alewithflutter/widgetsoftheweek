import 'package:flutter/material.dart';

class ExpandedWidget extends StatefulWidget {
  

  @override
  _ExpandedWidgetState createState() => _ExpandedWidgetState();
}

class _ExpandedWidgetState extends State<ExpandedWidget> {
  
  bool showExpandedWidget = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 20
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  //container 1
                  showExpandedWidget ? containerWithExpanded() : containerWithoutExpanded(),
                  SizedBox(width: 10),
                  Container(
                    padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(5)
                    ),
                    child: Icon(Icons.add),
                  ),
                  SizedBox(width: 10),
                  //container 2
                  Container(
                    padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(5)
                    ),
                    child: Icon(Icons.add),
                  )
                ],
              ),
              SizedBox(height: 10),
              Material(
                child: InkWell(
                  onTap: ()=>setState(()=> showExpandedWidget = !showExpandedWidget),
                  splashColor: Colors.orange,
                  highlightColor: Colors.orange,
                  child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(
                      vertical: 20,
                    ),
                    color: Colors.blue[900].withOpacity(0.9),
                    child: Text('Mostrar Expanded Widget', 
                      style: TextStyle(
                        color: Colors.white
                      ),
                    ),
                  ),
                ),
              )
            ],
          )
        ),
      ),
    );
  }

  Widget containerWithExpanded(){
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.circular(5)
        ),
        padding: EdgeInsets.all(20),
        child: Text('Buy it now!', style: TextStyle(
          fontSize: 20,
          color: Colors.white,
          fontWeight: FontWeight.w700
        )),
      ),
    );
  }
  Widget containerWithoutExpanded(){
    return Container(
      decoration: BoxDecoration(
        color: Colors.red,
        borderRadius: BorderRadius.circular(5)
      ),
      padding: EdgeInsets.all(20),
      child: Text('Buy it now!', style: TextStyle(
        fontSize: 20,
        color: Colors.white,
        fontWeight: FontWeight.w700
      )),
    );
  }
}